Rails.application.routes.draw do
  
  post "/sessions",           to: "authentication#login"
  post "/users",              to: "users#create"
  get "/users/:id",           to: "users#show"
  patch "/users/:id",         to: "users#update"
  get  "/auto_login",         to: "users#auto_login"
  resources :jobs do
    resource :apply
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
