namespace :dev do
  desc "TODO"
  task setup: :environment do

    # p "Criando Users"

    # User.create!(
    #   "email": "filipe@example.com",
    #   "name": "filipe",
    #   "phone": "xxxxxx",
    #   "birthdate": "01/01/1994",
    #   "password": "123456"
    # )

    # p "Users criados"

    p "Criando Jobs"

    Job.create!(
      "name": "DevOps",
      "summary": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros vestibulum ipsum congue venenatis ut ac nibh. Phasellus ultrices vulputate velit a dictum. Duis commodo erat ex, vel elementum leo condimentum sed. Praesent pulvinar hendrerit dolor, sit amet maximus nisi. Mauris in elementum elit, at tincidunt urna. Quisque at nisl aliquet, pellentesque diam non, vestibulum ex. Nullam cursus at nisi et varius. Donec fringilla condimentum ornare. In at tellus ex.",
      "requirements": "Aliquam sodales elementum ex, vel sagittis magna blandit et. Suspendisse varius maximus quam ac consequat.",
      "responsibilities": "Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos."
    )

    Job.create!(
      "name": "Analista de Testes/QA",
      "summary": "Maecenas ac eros vestibulum ipsum congue venenatis ut ac nibh.",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros vestibulum ipsum congue venenatis ut ac nibh. Phasellus ultrices vulputate velit a dictum. Duis commodo erat ex, vel elementum leo condimentum sed. Praesent pulvinar hendrerit dolor, sit amet maximus nisi. Mauris in elementum elit, at tincidunt urna. Quisque at nisl aliquet, pellentesque diam non, vestibulum ex. Nullam cursus at nisi et varius. Donec fringilla condimentum ornare. In at tellus ex.",
      "requirements": "Aliquam sodales elementum ex, vel sagittis magna blandit et. Suspendisse varius maximus quam ac consequat.",
      "responsibilities": "Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos."
    )

    Job.create!(
      "name": "Desenvolvedor Ruby on Rails",
      "summary": "Phasellus ultrices vulputate velit a dictum.",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros vestibulum ipsum congue venenatis ut ac nibh. Phasellus ultrices vulputate velit a dictum. Duis commodo erat ex, vel elementum leo condimentum sed. Praesent pulvinar hendrerit dolor, sit amet maximus nisi. Mauris in elementum elit, at tincidunt urna. Quisque at nisl aliquet, pellentesque diam non, vestibulum ex. Nullam cursus at nisi et varius. Donec fringilla condimentum ornare. In at tellus ex.",
      "requirements": "Aliquam sodales elementum ex, vel sagittis magna blandit et. Suspendisse varius maximus quam ac consequat.",
      "responsibilities": "Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos."
    )
  
    p "Jobs criados"

  end

end
