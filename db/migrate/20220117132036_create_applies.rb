class CreateApplies < ActiveRecord::Migration[6.1]
  def change
    create_table :applies do |t|
      t.references    :user, null:true, foreign_key:true
      t.references    :job, null:true, foreign_key:true

      t.timestamps  
    end
  end
end
