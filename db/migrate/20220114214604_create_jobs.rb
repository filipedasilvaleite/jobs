class CreateJobs < ActiveRecord::Migration[6.1]
  def change
    create_table :jobs do |t|
      t.string :name
      t.text :summary
      t.text :description
      t.text :requirements
      t.text :responsibilities

      t.timestamps
    end
  end
end
