class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]
  before_action :authorize_request, except: :create
  before_action :find_user, except: %i[create index]
  # before_action :authorized, only: [:auto_login, :show]

  # GET /users
  def index
    @users = User.all

    render json: @users
  end

  # GET /users/1
  def show
    payload = { user_id: @user.attributes[:id], email: @user.attributes[:email]}
    token   = JsonWebToken.encode(payload)

    authentication_token = { "authentication_token" => token }
    
    render json: { status: 0, data: @user.attributes.except("password", "password_digest", "created_at", "updated_at")}


    # render json: { status: 0, data: @user}, except: [ :password, :password_digest, :created_at, :updated_at ]
  end

  # POST /users
  def create
    @user = User.new(user_params)
    if @user.save
      user_id = User.find_by( email: params[:email] ).id
      payload = { user_id: user_id, email: params[:email]}
      token   = JsonWebToken.encode(payload)

      authentication_token = { "authentication_token" => token }
      
      render json: { status: 0, data: @user.attributes.merge(authentication_token).except("password", "password_digest", "created_at", "updated_at")}
      # user_id = User.find_by( email: params[:email] ).id
      # payload = { user_id: user_id, email: params[:email]}
      # token   = encode_token(payload)

      # authentication_token = { "authentication_token" => token }
      
      # render json: { status: 0, data: @user.attributes.merge(authentication_token).except("password", "password_digest", "created_at", "updated_at")}


    else
      render json: { errors: @user.errors.full_messages },
      status: :unprocessable_entity
      # render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      
      render json: { status: 0, data: @user }, except: [ :password, :created_at, :updated_at, :password_digest ]
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  # LOGGING IN
  def login
    @user = User.find_by(email: params[:email])

    if @user && @user.authenticate(params[:password])
      user_id = User.find_by( email: params[:email] ).id
      payload = { user_id: user_id, email: params[:email]}
      token   = encode_token(payload)

      authentication_token = { "authentication_token" => token }

      render json: { status: 0, data: @user.attributes.merge(authentication_token).except("password", "password_digest", "created_at", "updated_at") }
    else 
      render json: {error: "Invalid username or password"}
    end
  end

  def auto_login
    render json: @user
  end

  private

    def find_user
      @user = User.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        render json: { errors: 'User not found' }, status: :not_found
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(:email, :username, :phone, :birthdate, :password)
    end
end