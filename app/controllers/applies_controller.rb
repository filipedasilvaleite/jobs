class AppliesController < ApplicationController
  before_action :authorize_request
  before_action :set_apply, only: [ :destroy ]

  def create
    # user_params = decode_token(params[:authentication_token])

    # apply_params = { "user_id" => params[:user_id], "job_id" => params[:job_id] }
    # @apply = Apply.new(apply_params)

    user_params = JsonWebToken.decode(params[:authentication_token])
    apply_params = { "user_id" => params[:user_id], "job_id" => params[:job_id] }

    @apply = Apply.new(apply_params)

    if @apply.save
      render json: { status: 0, data: "Você se postulou à vaga de #{Job.find(apply_params["job_id"]).name}. Boa sorte!"}
    else
      render json: @apply.errors, status: :unprocessable_entity
    end
  end

    # DELETE /apply/1
    def destroy
      begin @apply.destroy
        render json: { status:0, data: "Sua postulação foi removida"}
      rescue
        render json: { data: "Alguma mensagem deveria aparecer aqui"}
      end
    end

  private

    def set_apply
      @apply = Apply.find_by("user_id": params[:user_id])
    end
    

    # Only allow a list of trusted parameters through.
    def apply_params
      params.require(:apply).permit(:user_id, :job_id)
    end

end