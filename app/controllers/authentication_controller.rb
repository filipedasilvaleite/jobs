class AuthenticationController < ApplicationController

    before_action :authorize_request, except: :login

    # POST /sessions
    def login
      @user = User.find_by_email(params[:email])
      if @user&.authenticate( params[:password] )
        payload = { user_id: User.find_by( email: params[:email]).id, email: params[:email]}
        token = JsonWebToken.encode(payload)

        authentication_token = { "authentication_token" => token }
        
        render json: { status: 0, data: @user.attributes.merge(authentication_token).except("password", "password_digest", "created_at", "updated_at") }
      else
        render json: { error: 'unauthorized' }, status: :unauthorized
      end
    end
  
    private
  
    def login_params
      params.permit(:email, :password)
    end
end
