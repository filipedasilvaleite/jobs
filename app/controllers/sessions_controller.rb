class SessionsController < ApplicationController
    
    def create
        hmac_secret = 'mySecretKey'
        payload = { email: session_params[:email], password: session_params[:password] }
        token = JWT.encode payload, hmac_secret,'HS256'
        token = { "authentication_token" => token }

        begin
            @user = User.find_by(email: session_params[:email], password: session_params[:password])

            render json: @user, status: :created, location: @user
        rescue 
            render json: { message: "Email ou senha não conferem"} 
        end
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_session
      @session = Session.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def session_params
      params.require(:session).permit(:email, :password)
    end
end
